namespace SQLClient.Console;
using SQLClient.Model;
using SQLClient.Repositories;
using System;

public class AddCustomer: ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {
        Console.WriteLine("Add customer");
        Console.Write("Firstname: ");
        var firstName = Console.ReadLine() ?? "";
        Console.Write("\nLastname: ");
        var lastName = Console.ReadLine() ?? "";
        Console.Write("\nEmail Address: ");
        var emailAddress = Console.ReadLine() ?? "";
        var customer = new Customer
        (
          FirstName: firstName,
          LastName: lastName,
          Email: emailAddress
        );
        repo.Add(customer);
    }
}