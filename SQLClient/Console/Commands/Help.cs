namespace SQLClient.Console;
using System;
public class Help : ICommand
{
    private static string[] availableCommands = AppDomain.CurrentDomain
        .GetAssemblies()
        .SelectMany(a => a.GetTypes())
        .Where(t => typeof(ICommand).IsAssignableFrom(t) && !t.IsInterface)
        .Select(t => t.Name).ToArray();
    public void Run()
    {
        string availableCommand;
        System.Console.WriteLine("Available Commands: ");
        for(var i=0; i<availableCommands.Length; i++)
        {
            availableCommand = availableCommands[i];
            Console.WriteLine($"{i+1}:\t{availableCommand}");
        }
    }
}