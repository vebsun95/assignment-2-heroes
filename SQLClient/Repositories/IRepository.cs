namespace SQLClient.Repositories;

public interface IRepository<T>
{
  /// <summary>
  /// Adds a new customer to the database.
  /// </summary>
  /// <param name="newItem">The new customers with its customer objects.</param>
  /// <returns>A boolean value where True = Successfull execution.</returns>
  public bool Add(T newItem);

  /// <summary>
  /// Reads all the customers in the database, and displays all the customer objects.
  /// </summary>
  /// <returns>A list of all customers in the database</returns>
  public IEnumerable<T> GetAll();

  /// <summary>
  /// Reads a spesific customer from the database, by its ID. Writes to console if ID is not found.
  /// </summary>
  /// <param name="id">The spesific customers ID.</param>
  /// <returns>A customer with the given ID displaying all the Customer objects.</returns>
  public T? GetById(int id);

  /// <summary>
  /// Reads a spesific customer by name. Partial match will also read a customer. Writes to console if name is an empty string or 
  /// no one with the name is found.
  /// </summary>
  /// <param name="name">The customers firstname and/or lastname.</param>
  /// <returns>A customer with the given name displaying all the Customer objects.</returns>
  public T? GetByName(string name);

  /// <summary>
  /// Reads a page of customers from the database with given limit and offset.
  /// </summary>
  /// <param name="limit">Sets a limit of numbers of rows returned</param>
  /// <param name="offset">Skips the offset rows before beginning to query.</param>
  /// <returns>A list of customers within the given limit and offset. </returns>
  public IEnumerable<T> GetPageOfItems(int limit, int offset);

  /// <summary>
  /// Updates an already existing customer in the database.
  /// </summary>
  /// <param name="updated">The customer with its new customer objects.</param>
  /// <returns>A boolean valu where True = Successfull update.</returns>
  public bool Update(T updated);

}
