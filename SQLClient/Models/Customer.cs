﻿namespace SQLClient.Model;

/// <summary>
/// Customer class with checks that validates customers input values, and throws exceptions.
/// </summary>
public class Customer
{
  public int CustomerId { get; set; }
  public string FirstName { get; set; }
  public string LastName { get; set; }
  public string? Company { get; set; }
  public string? Address { get; set; }
  public string? City { get; set; }
  public string? State { get; set; }
  public string? Country { get; set; }
  public string? PostalCode { get; set; }
  public string? Phone { get; set; }
  public string? Fax { get; set; }
  public string Email { get; set; }
  
  public Customer(int id, string FirstName, string LastName, string Email, string? Company = null,
    string? Address = null, string? City = null, string? State = null, string? Country = null, string? PostalCode = null, string? Phone = null, string? Fax = null)
  {
    if (BadId(id)) throw new ArgumentException("The Id has to be a postive value");
    else if (BadFirstName(FirstName)) throw new ArgumentException("A FirstName has to be more than 0 charachters long");
    else if (BadLastName(LastName)) throw new ArgumentException("A LastName has to be more than 0 charachters long");
    else if (BadEmail(Email)) throw new ArgumentException("Email is not formatted correctly must include @ and atleast 2 other letters");

    CustomerId = id;
    this.FirstName = FirstName;
    this.LastName = LastName;
    this.Email = Email;
    this.Company = Company;
    this.Address = Address;
    this.City = City;
    this.State = State;
    this.PostalCode = PostalCode;
    this.Fax = Fax;
  }

  public Customer(string FirstName, string LastName, string Email, string? Company = null,
    string? Address = null, string? City = null, string? State = null, string? Country = null, string? PostalCode = null, string? Phone = null, string? Fax = null)
  {
    if (BadFirstName(FirstName)) throw new ArgumentException("A FirstName has to be more than 0 charachters long");
    else if (BadLastName(LastName)) throw new ArgumentException("A LastName has to be more than 0 charachters long");
    else if (BadEmail(Email)) throw new ArgumentException("Email is not formatted correctly must include @ and atleast 2 other letters");

    this.FirstName = FirstName;
    this.LastName = LastName;
    this.Email = Email;
    this.Company = Company;
    this.Address = Address;
    this.City = City;
    this.State = State;
    this.PostalCode = PostalCode;
    this.Fax = Fax;
  }

  public static bool HasCorrectRequiredValues(Customer customer)
  {
    if (BadEmail(customer.Email)) return false;
    else if (BadId(customer.CustomerId)) return false;
    else if (BadFirstName(customer.FirstName)) return false;
    else if (BadLastName(customer.LastName)) return false;
    return true;
  }

  private static bool BadFirstName(string firstName) { return firstName.Length < 1; }
  private static  bool BadLastName(string lastName) { return lastName.Length < 1; }
  public static bool BadId(int Id) { return Id < 0; }
  private static bool BadEmail(string email)
  {
    return !email.Contains('@') && email.Length < 3;
  }


  public override string ToString()
  {
    return $"ID: {CustomerId}\n FirstName: {FirstName}\n LastName: {LastName}\n Country: {Country}\n Postal Code: {PostalCode}\n Email: {Email}";

  }
}