
namespace SQLClient.Console;
using SQLClient.Repositories;
using System;

public class GetCustomerByName: ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {
        Console.WriteLine("Get customer by name: ");
        Console.Write("Name: ");
        var name = Console.ReadLine() ?? "";
        var customer = repo.GetByName(name);
        Console.WriteLine(customer);
    }
}