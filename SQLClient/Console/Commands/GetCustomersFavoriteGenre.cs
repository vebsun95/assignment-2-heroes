namespace SQLClient.Console;
using SQLClient.Repositories;
using System;

public class GetCustomersFavoriteGenre: ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {

        int id;
        Console.Write("Get customer by Id: ");
        var input = Console.ReadLine();
        if(int.TryParse(input, out id))
        {
            var customer = repo.GetById(id);
            var favoriteGenre = repo.GetCustomersFavoriteGenre(customer);
            foreach(var favGenre in favoriteGenre)
            {
                Console.WriteLine($"Genre: {favGenre.genre}\t # of tracks: {favGenre.nrOfTracks}");
            }
        }
        else
            Console.WriteLine($"Could not parse the int: {input}");        
    }
}