﻿namespace SQLClient;
using System.Data.SqlClient;

public class ChinookDbConfig
{
  public static string GetConnectionString()
  {
    SqlConnectionStringBuilder builder = new();
    builder.DataSource = "(localdb)\\MSSQLLocalDB";
    builder.InitialCatalog = "Chinook";
    builder.IntegratedSecurity = true;
    return builder.ConnectionString;
  }
}
