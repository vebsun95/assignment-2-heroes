﻿using SQLClient.Model;
namespace SQLClient.Repositories;

interface ICustomerRepository : IRepository<Customer>
{
  /// <summary>
  /// Gets the number of customers in each country in a descending order.
  /// </summary>
  /// <returns>A list of countries and the customer count.</returns>
  List<(string country, int count)> GetNumberOfCustomersPerCountry();

  /// <summary>
  /// Gets the highest spending customers in a descending order.
  /// </summary>
  /// <returns>A list of customers, and the customers spending.</returns>
  public List<(Customer customer, decimal spending)> GetHighestSpenders();

  /// <summary>
  /// For a given customer, gets their most popular genre that corresponds with their tracks in their invoices.
  /// </summary>
  /// <param name="customer">Given customer</param>
  /// <returns>A list of most popular genres and the number of tracks in this genre.</returns>
  public List<(string genre, int nrOfTracks)> GetCustomersFavoriteGenre(Customer customer);

}