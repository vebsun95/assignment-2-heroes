namespace SQLClient.Console;
using System;
public class Clear: ICommand
{
    public void Run()
    {
        Console.Clear();
    }
}