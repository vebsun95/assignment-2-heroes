﻿namespace SQLClient.Model;

public class Album
{
    public int    AlbumId { get; set; }
    public string Title{ get; set; }
}