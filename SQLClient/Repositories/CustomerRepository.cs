﻿namespace SQLClient.Repositories;
using SQLClient.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;


public class CustomerRepository : ICustomerRepository
{
  public bool Add(Customer newItem)
  {
    try
    {
      using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
      connection.Open();

      string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@firstname, @lastname, @country, @postalcode, @phone, @email)";

      using SqlCommand cmd = new SqlCommand(sql, connection);

      cmd.Parameters.AddWithValue("@firstname", newItem.FirstName);
      cmd.Parameters.AddWithValue("@lastname", newItem.LastName);
      cmd.Parameters.AddWithValue("@country", newItem.Country ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@postalcode", newItem.PostalCode ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@phone", newItem.Phone ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@email", newItem.Email);

      return cmd.ExecuteNonQuery() == 1;
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }
    return false;
  }

  public IEnumerable<Customer> GetAll()
  {
    List<Customer> customers = new();
    try
    {

      using SqlConnection connection = new(ChinookDbConfig.GetConnectionString());
      connection.Open();

      string sql = "SELECT * FROM Customer";
      using SqlCommand cmd = new(sql, connection);

      using SqlDataReader myreader = cmd.ExecuteReader();
      {
        while (myreader.Read())
        {
          customers.Add(ReaderToCustomer(myreader));
        }
      }
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }
    return customers;

  }

  private Customer ReaderToCustomer(SqlDataReader myreader)
  {
    Customer customer = new(
      id: myreader.GetInt32(0),
      FirstName: myreader.GetString(1),
      LastName: myreader.GetString(2),
      Company: myreader.IsDBNull(3) ? null : myreader.GetString(3),
      Address: myreader.IsDBNull(4) ? null : myreader.GetString(4),
      City: myreader.IsDBNull(5) ? null : myreader.GetString(5),
      State: myreader.IsDBNull(6) ? null : myreader.GetString(6),
      Country: myreader.IsDBNull(7) ? null : myreader.GetString(7),
      PostalCode: myreader.IsDBNull(8) ? null : myreader.GetString(8),
      Phone: myreader.IsDBNull(9) ? null : myreader.GetString(9),
      Fax: myreader.IsDBNull(10) ? null : myreader.GetString(10),
      Email: myreader.GetString(11)
    );
    return customer;
  }

  public Customer? GetById(int id)
  {
    Customer? customer = null;
    try
    {
      if (Customer.BadId(id)) throw new ArgumentException("The Index should be a postive number");
      using SqlConnection connection = new(ChinookDbConfig.GetConnectionString());
      connection.Open();
      string sql = "SELECT * FROM Customer WHERE CustomerId = @id";
      using SqlCommand cmd = new(sql, connection);
      cmd.Parameters.AddWithValue("@id", id);
      using SqlDataReader myreader = cmd.ExecuteReader();
      if (myreader.Read())
        customer = ReaderToCustomer(myreader);
      else
      {
        throw new ArgumentException("Noone by that ID");
      }
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }
    return customer;
  }

  public Customer? GetByName(string fullName)
  {
    fullName = fullName.Trim();
    Customer? customer = null;
    try
    {
      if (fullName == String.Empty)
      {
        throw new ArgumentException("Must enter a name");
      }
      string[] NameStrings = fullName.Split(' ');
      string FirstName = NameStrings[0];
      string LastName = NameStrings[NameStrings.Length - 1];
      using SqlConnection connection = new(ChinookDbConfig.GetConnectionString());
      connection.Open();
      string sql = "SELECT * FROM Customer WHERE FirstName LIKE '%' + @firstName + '%' OR LastName LIKE '%' + @lastName + '%'";
      using SqlCommand cmd = new(sql, connection);
      cmd.Parameters.AddWithValue("@firstName", FirstName);
      cmd.Parameters.AddWithValue("@lastName", LastName);
      using SqlDataReader myreader = cmd.ExecuteReader();
      if (myreader.Read())
        customer = ReaderToCustomer(myreader);
      else
      {
        throw new ArgumentException("Noone by that Name");
      }
      customer = ReaderToCustomer(myreader);
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }

    return customer;
  }

  public bool Update(Customer updated)
  {
    try
    {
      if (Customer.BadId(updated.CustomerId)) throw new ArgumentException("The Index should be a postive number");
      using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
      connection.Open();

      string sql = @"UPDATE Customer SET FirstName = @NewFirstName, LastName = @LastName, 
        Email = @Email, State = @State, City = @City, Phone = @Phone, Address = @Address, 
        @Country = Country, Company = Company, Fax = @Fax, PostalCode = @PostalCode 
        WHERE CustomerId = @Id";

      using SqlCommand cmd = new SqlCommand(sql, connection);
      cmd.Parameters.AddWithValue("@NewFirstName", updated.FirstName);
      cmd.Parameters.AddWithValue("@LastName", updated.LastName);
      cmd.Parameters.AddWithValue("@Email", updated.Email);
      cmd.Parameters.AddWithValue("@State", updated.State ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@City", updated.City ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@Phone", updated.Phone ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@Address", updated.Address ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@Company", updated.Company ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@Country", updated.Country ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@Fax", updated.Fax ?? SqlString.Null);
      cmd.Parameters.AddWithValue("@PostalCode", updated.PostalCode ?? SqlString.Null);
      //WHERE parameter
      cmd.Parameters.AddWithValue("@Id", updated.CustomerId);

      return cmd.ExecuteNonQuery() == 1;
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }
    return false;
  }

  public IEnumerable<Customer> GetPageOfItems(int limit, int offset)
  {
    List<Customer> customers = new();
    try
    {
      using SqlConnection connection = new(ChinookDbConfig.GetConnectionString());
      connection.Open();

      string sql = "SELECT * FROM Customer ORDER BY customerid ASC OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";
      using SqlCommand cmd = new(sql, connection);
      cmd.Parameters.AddWithValue("@Offset", offset);
      cmd.Parameters.AddWithValue("@Limit", limit);

      using SqlDataReader myreader = cmd.ExecuteReader();
      {
        while (myreader.Read())
        {
          customers.Add(ReaderToCustomer(myreader));
        }
      }
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }
    return customers;
  }

  public List<(string country, int count)> GetNumberOfCustomersPerCountry()
  {
    List<(string country, int count)> numberOfCustomersPerCountry = new();
    try
    {
      using SqlConnection connection = new(ChinookDbConfig.GetConnectionString());
      connection.Open();
      string sql = "SELECT Country, COUNT(*) as Count FROM Customer GROUP BY Country ORDER BY Count DESC";
      using SqlCommand cmd = new(sql, connection);
      using SqlDataReader myreader = cmd.ExecuteReader();
      while (myreader.Read())
      {
        numberOfCustomersPerCountry.Add((myreader.GetString(0), myreader.GetInt32(1)));
      }
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }
    return numberOfCustomersPerCountry;
  }

  public List<(Customer customer, decimal spending)> GetHighestSpenders()
  {
    List<(Customer customer, decimal spending)> HighestSpenders = new();

    try
    {
      using SqlConnection connection = new(ChinookDbConfig.GetConnectionString());
      connection.Open();
      string sql = @"SELECT Customer.CustomerId, SUM(Invoice.Total) as TotalInvoice, Customer.FirstName, Customer.LastName, 
                    Customer.Country, Customer.PostalCode, Customer.Phone, Customer.Email  
                    FROM Customer INNER JOIN Invoice ON Customer.CustomerId  = Invoice.CustomerId 
                    GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName, Customer.Country, Customer.PostalCode, 
                    Customer.Phone, Customer.Email
					          ORDER BY TotalInvoice DESC
";
      using SqlCommand cmd = new(sql, connection);

      using SqlDataReader myreader = cmd.ExecuteReader();
      while (myreader.Read())
      {
        var customer = new Customer(
          id: myreader.GetInt32(0),
          FirstName: myreader.GetString(2),
          LastName: myreader.GetString(3),
          Country: myreader.IsDBNull(4) ? null : myreader.GetString(4),
          PostalCode: myreader.IsDBNull(5) ? null : myreader.GetString(5),
          Phone: myreader.IsDBNull(6) ? null : myreader.GetString(6),
          Email: myreader.GetString(7)
        );

        HighestSpenders.Add((customer, myreader.GetDecimal(1)));
      }
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }
    return HighestSpenders;
  }

  public List<(string genre, int nrOfTracks)> GetCustomersFavoriteGenre(Customer customer)
  {
    List<(string genre, int nrOfTracks)> favoriteGenre = new();
    try
    {
      using SqlConnection connection = new(ChinookDbConfig.GetConnectionString());
      connection.Open();
      string sql = @"
        with B as(
	        SELECT Genre.Name, SUM(InvoiceLine.Quantity) AS SumQuantity FROM InvoiceLine
	        INNER JOIN Invoice
	        ON Invoice.InvoiceId = InvoiceLine.InvoiceId
	        INNER JOIN Track
	        ON Track.TrackId = InvoiceLine.TrackId
	        INNER JOIN Genre
	        ON Genre.GenreId = Track.GenreId
	        INNER JOIN Customer
	        ON Customer.CustomerId = Invoice.CustomerId
	        WHERE Invoice.CustomerId = @customerId
	        GROUP BY Genre.Name, Track.GenreId
        )
        select * FROM B WHERE SumQuantity = (select Max(SumQuantity) FROM B );";
      using SqlCommand cmd = new(sql, connection);
      cmd.Parameters.AddWithValue("@customerId", customer.CustomerId);
      using SqlDataReader myreader = cmd.ExecuteReader();
      while (myreader.Read())
      {
        favoriteGenre.Add((myreader.GetString(0), myreader.GetInt32(1)));
      }
    }
    catch (SqlException ex)
    {
      Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
      Console.WriteLine(ex.Message);
    }
    return favoriteGenre;
  }
}

