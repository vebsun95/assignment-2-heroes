namespace SQLClient.Console;
using SQLClient.Repositories;
using System;

public class GetNumberOfCustomersPerCountry : ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {
        var customersPerCountry = repo.GetNumberOfCustomersPerCountry();
        foreach(var tuple in customersPerCountry)
            Console.WriteLine($"{tuple.country}:  {tuple.count}");    
    }
}