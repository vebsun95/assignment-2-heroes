namespace SQLClient.Console;
using SQLClient.Repositories;

public class GetAllCustomers : ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {
        var customers = repo.GetAll();
        foreach(var customer in customers)
            System.Console.WriteLine(customer);    
    }
}