namespace SQLClient.Console;
using SQLClient.Repositories;
using System;

public class GetCustomerByID: ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {
        int id;
        Console.Write("Get customer by Id: ");
        var input = Console.ReadLine();
        if(int.TryParse(input, out id))
        {
            var customer = repo.GetById(id);
            System.Console.WriteLine(customer);
        }
        else
            Console.WriteLine($"Could not parse the int: {input}");        
    }
}