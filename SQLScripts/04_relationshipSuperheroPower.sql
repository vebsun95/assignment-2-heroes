Create TABLE Power_Superhero (
	HeroId int FOREIGN KEY REFERENCES Power(Id),
	PowerId int FOREIGN KEY REFERENCES Superhero(Id),
	PRIMARY KEY (HeroId, PowerId)
)
