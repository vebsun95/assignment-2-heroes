namespace SQLClient.Console;
using System;
public class Exit : ICommand
{
    public void Run()
    {
        Console.WriteLine("Exiting ...");
        Environment.Exit(0);
    }
}