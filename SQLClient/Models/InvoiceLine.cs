﻿namespace SQLClient.Model;

public class InvoiceLine
{
    public int    InvoiceLineId { get; set; }
    public double UnitPrice     { get; set; }
    public int    Quantity      { get; set; }
}