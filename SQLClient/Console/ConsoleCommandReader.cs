namespace SQLClient.Console;
using System;

public interface ICommand
{
    void Run();
}

public class ConsoleCommandReader
{
    private Dictionary<string, ICommand> _commands = new();
    public ConsoleCommandReader()
    {
        var types = AppDomain.CurrentDomain
            .GetAssemblies()
            .SelectMany(a => a.GetTypes())
            .Where(t => typeof(ICommand)
            .IsAssignableFrom(t) && !t.IsInterface).ToList();
        for(var i=0; i < types.Count; i++)
        {
            var type = types[i];
            var command = Activator.CreateInstance(type) as ICommand;
            if(command is not null)
            {
                _commands.Add(type.Name.Replace("Command", "").ToLower(), command);
                _commands.Add((i+1).ToString(), command);
            }
        }
    }
    public void Start()
    {
        string command;
        while(true)
        {
            command = Console.ReadLine() ?? "";
            Console.Write("\n\n\n");
            command = command.ToLower();
            if(_commands.ContainsKey(command))
                _commands[command].Run();
            else
                System.Console.WriteLine("Did not recorecogniz the command");
        }
    }
}