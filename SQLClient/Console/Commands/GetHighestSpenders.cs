namespace SQLClient.Console;
using SQLClient.Repositories;
using System;

public class GetHighestSpenders: ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {
        var customers = repo.GetHighestSpenders();
        foreach(var customer in customers)
        {
            Console.WriteLine($"{customer.customer} {customer.spending}");
        }
    }
}