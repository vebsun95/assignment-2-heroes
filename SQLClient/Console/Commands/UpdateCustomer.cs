namespace SQLClient.Console;
using SQLClient.Repositories;
using System;

public class UpdateCustomer : ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {
        int id;
        Console.Write("Enter the customer Id: ");
        var input = Console.ReadLine();
        if(int.TryParse(input, out id))
        {
            var customer = repo.GetById(id);
            if(customer is null)
            {
                Console.WriteLine($"Could not find customer with Id: {id}");
                return;
            }
            Console.Write("Enter new Email address: ");
            var emailAddress = Console.ReadLine() ?? "";
            customer.Email = emailAddress;
            repo.Update(customer);
        }
        else
            Console.WriteLine($"Could not parse the int: {input}");        
    }
}