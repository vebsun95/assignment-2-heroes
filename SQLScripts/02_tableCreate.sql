﻿
CREATE TABLE Superhero
(
	Id int IDENTITY(1, 1) PRIMARY KEY,
	Name nvarchar(80) NOT NULL,
	Alias nvarchar(255),
	Origin nvarchar(2000),
);

CREATE TABLE Assistant
(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(80) NOT NULL,
);


CREATE TABLE Power(
	Id int IDENTITY(1,1) PRIMARY KEY ,
	Name nvarchar(80) NOT NULL,
	Description nvarchar(3000)
);



