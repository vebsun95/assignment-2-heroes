namespace SQLClient.Console;
using SQLClient.Repositories;
using System;

public class GetPageOfCustomers: ICommand
{
    private static ICustomerRepository repo = new CustomerRepository();
    public void Run()
    {
        int offset; int limit;
        Console.WriteLine("Get Page Of Customers");
        Console.Write("Offset: ");
        var inputOffset = Console.ReadLine() ?? "";
        Console.Write("\nLimit: ");
        var inputLimit = Console.ReadLine() ?? "";
        if(int.TryParse(inputOffset, out offset) && int.TryParse(inputLimit, out limit))
        {
            var customers = repo.GetPageOfItems(limit, offset);
            foreach(var customer in customers)
                Console.WriteLine(customer);
        }
    }
}