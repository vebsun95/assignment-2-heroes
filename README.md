# Assignment 2: Create a database and access it

## Description
This assignment is broken into two parts
- The first is the SQLScripts folder which contains the needed scripts to create a small database. 
- The secound is a Console application which is used to perform CRUD repository actions and more on a music database.

## Table of Contents
 - [Contributors](#contributors)
 - [Dependencies](#dependencies)
 - [Installation](#installation)
 - [Usage](#usage)
 - [Contributing](#Contributing)
 - [License](#license)

## Contributors
Øyvind Sande Reitan <https://gitlab.com/Hindrance>  
Vebjørn Sundal <https://gitlab.com/vebsun95>  
Synne Sævik <https://gitlab.com/Synnems>

## Dependencies
- NugetPackageManager, Package System.Data.SqlClient(4.8.3)
- Chinook.sql script which sets up a database
- .Net 6
- Microsoft SQL

## Installation
You will need to set up a MSSQL server and connect to it before you use these
The scripts in the folder SQLClient can then interact with the server. You should run the scripts in order 1-9 they will then create a database with the tables seen in the class diagram.
### SuperHero DB
![Diagram2](../SuperHeroDiagram.PNG)  
### Music DB
This one represents the music database used in the repository part of the assignment  
![Diagram](../Assignment-2-diagram.png)  
## Usage
Run the SQLClient project, and enter `Help` into the terminal

## Contributing
You cannot contribute to this project, as it is a school assignment

## License
[Apache V2](../LICENSE)
