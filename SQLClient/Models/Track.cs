﻿namespace SQLClient.Model;

public class Track
{
    public int     TrackId { get; set; }
    public string  Name { get; set; }
    public string? Composer { get; set; }
    public int     Milliseconds { get; set; }
    public int?    Bytes { get; set; }
    public double  UnitPrice { get; set; }
}